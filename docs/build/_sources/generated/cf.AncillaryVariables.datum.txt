cf.AncillaryVariables.datum
===========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AncillaryVariables.datum