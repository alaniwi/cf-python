cf.AncillaryVariables.measures
==============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AncillaryVariables.measures