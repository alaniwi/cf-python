.. currentmodule:: cf
.. default-role:: obj

.. highlight:: bash

Installation
============

Install with pip
----------------

`cf` is in `PyPI <https://pypi.python.org/pypi/cf-python>`_::

   $ pip install cf-python

Install from source
-------------------

Downloads are available from `PyPI
<https://pypi.python.org/pypi/cf-python>`_ or from the `bitbucket
repository <https://bitbucket.org/cfpython/cf-python/>`_.
Installation instructions and dependencies are in the `README.md
<https://bitbucket.org/cfpython/cf-python/src/master/README.md>`_
file.

Issues
------

Please raise any questions or problems through the `issue tracker
<https://bitbucket.org/cfpython/cf-python/issues>`_.
