cf.Data.__nonzero__
===================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Data.__nonzero__