.. currentmodule:: cf
.. default-role:: obj

cf.Partition
============

.. autoclass:: cf.Partition
   :no-members:
   :no-inherited-members:

Partition attributes
--------------------

.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~cf.Partition.in_memory
   ~cf.Partition.indices
   ~cf.Partition.isscalar
   ~cf.Partition.on_disk

Partition methods
-----------------
     
.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~cf.Partition.change_axis_names
   ~cf.Partition.close
   ~cf.Partition.copy
   ~cf.Partition.dataarray
   ~cf.Partition.file_close
   ~cf.Partition.is_partition
   ~cf.Partition.master_ndindex
   ~cf.Partition.ndindex
   ~cf.Partition.new_part
   ~cf.Partition.overlaps
   ~cf.Partition.to_disk
   ~cf.Partition.update_inplace_from

