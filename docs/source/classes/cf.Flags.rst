.. currentmodule:: cf
.. default-role:: obj

cf.Flags
========

.. autoclass:: Flags
   :no-members:
   :no-inherited-members:

Flags attributes
----------------
   
.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: attribute.rst

   ~Flags.flag_masks
   ~Flags.flag_meanings
   ~Flags.flag_values

Flags methods
-------------
   
.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~Flags.copy
   ~Flags.dump
   ~Flags.equals
   ~Flags.sort
