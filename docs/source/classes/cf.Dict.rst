.. currentmodule:: cf
.. default-role:: obj

cf.Dict
=========

.. autoclass:: cf.Dict
   :no-members:
   :no-inherited-members:

Dict methods
------------
   
.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~cf.Dict.copy
   ~cf.Dict.equals

Dict dict-like methods
----------------------

These methods provide functionality exactly as their counterparts in a
built-in :py:obj:`dict`.

.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~cf.Dict.clear
   ~cf.Dict.get
   ~cf.Dict.has_key
   ~cf.Dict.items
   ~cf.Dict.iteritems
   ~cf.Dict.iterkeys
   ~cf.Dict.itervalues
   ~cf.Dict.keys
   ~cf.Dict.pop
   ~cf.Dict.popitem
   ~cf.Dict.setdefault
   ~cf.Dict.update
   ~cf.Dict.values
