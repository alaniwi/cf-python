cf.AncillaryVariables.getprop
=============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AncillaryVariables.getprop