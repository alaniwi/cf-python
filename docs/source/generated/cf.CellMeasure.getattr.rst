cf.CellMeasure.getattr
======================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CellMeasure.getattr