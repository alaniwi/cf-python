cf.equals
=========

.. currentmodule:: cf
.. default-role:: obj

.. autofunction:: cf.equals