cf.Data.asreftime
=================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Data.asreftime