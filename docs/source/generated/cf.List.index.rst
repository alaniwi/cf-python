cf.List.index
=============

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.List.index