cf.Datetime.inspect
===================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Datetime.inspect