cf.CoordinateReference.setcoord
===============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateReference.setcoord