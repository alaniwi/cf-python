cf.FieldList.remove_data
========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.FieldList.remove_data