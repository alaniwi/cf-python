cf.Coordinate.unique
====================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Coordinate.unique