cf.CellMeasure.subspace
=======================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.CellMeasure.subspace