cf.Units.reftime
================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Units.reftime