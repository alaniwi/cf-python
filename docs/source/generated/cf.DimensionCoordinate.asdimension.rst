cf.DimensionCoordinate.asdimension
==================================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.DimensionCoordinate.asdimension