cf.Datetime.timetuple
=====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Datetime.timetuple