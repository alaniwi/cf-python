cf.Coordinate.setprop
=====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Coordinate.setprop