cf.AncillaryVariables.subspace
==============================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.AncillaryVariables.subspace