cf.FieldList.valid_range
========================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.FieldList.valid_range