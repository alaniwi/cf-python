cf.Domain.new_ref_identifier
============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Domain.new_ref_identifier