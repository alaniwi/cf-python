cf.CellMethods.equivalent
=========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CellMethods.equivalent