cf.Field.remove_axes
====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Field.remove_axes