cf.Domain.expand_dims
=====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Domain.expand_dims