cf.AncillaryVariables.remove_data
=================================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AncillaryVariables.remove_data