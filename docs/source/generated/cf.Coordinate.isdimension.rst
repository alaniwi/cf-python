cf.Coordinate.isdimension
=========================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Coordinate.isdimension