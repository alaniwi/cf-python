cf.Coordinate.close
===================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Coordinate.close