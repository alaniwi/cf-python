cf.CoordinateBounds.match
=========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateBounds.match