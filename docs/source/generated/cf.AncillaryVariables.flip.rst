cf.AncillaryVariables.flip
==========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AncillaryVariables.flip