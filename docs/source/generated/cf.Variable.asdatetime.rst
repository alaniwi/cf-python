cf.Variable.asdatetime
======================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Variable.asdatetime