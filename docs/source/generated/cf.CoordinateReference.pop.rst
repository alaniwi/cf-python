cf.CoordinateReference.pop
==========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateReference.pop