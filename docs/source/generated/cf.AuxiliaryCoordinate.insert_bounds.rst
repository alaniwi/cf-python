cf.AuxiliaryCoordinate.insert_bounds
====================================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AuxiliaryCoordinate.insert_bounds