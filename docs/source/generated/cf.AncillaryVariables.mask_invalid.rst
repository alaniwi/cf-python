cf.AncillaryVariables.mask_invalid
==================================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AncillaryVariables.mask_invalid