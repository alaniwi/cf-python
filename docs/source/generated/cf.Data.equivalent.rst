cf.Data.equivalent
==================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Data.equivalent