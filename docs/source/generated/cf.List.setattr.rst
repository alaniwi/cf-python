cf.List.setattr
===============

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.List.setattr