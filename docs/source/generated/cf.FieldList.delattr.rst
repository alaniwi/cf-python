cf.FieldList.delattr
====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.FieldList.delattr