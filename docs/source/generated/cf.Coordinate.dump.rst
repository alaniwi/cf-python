cf.Coordinate.dump
==================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Coordinate.dump