cf.DimensionCoordinate.subspace
===============================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.DimensionCoordinate.subspace