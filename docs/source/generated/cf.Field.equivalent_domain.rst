cf.Field.equivalent_domain
==========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Field.equivalent_domain