cf.Coordinate.transpose
=======================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Coordinate.transpose