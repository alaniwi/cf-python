cf.AncillaryVariables.binary_mask
=================================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AncillaryVariables.binary_mask