cf.CoordinateBounds.attributes
==============================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.CoordinateBounds.attributes