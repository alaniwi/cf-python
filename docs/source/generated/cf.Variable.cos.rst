cf.Variable.cos
===============

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Variable.cos