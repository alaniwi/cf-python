cf.Coordinate.hasprop
=====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Coordinate.hasprop