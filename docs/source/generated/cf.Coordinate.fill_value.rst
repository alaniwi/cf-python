cf.Coordinate.fill_value
========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Coordinate.fill_value