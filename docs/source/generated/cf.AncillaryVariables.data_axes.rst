cf.AncillaryVariables.data_axes
===============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AncillaryVariables.data_axes