cf.Variable.equals
==================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Variable.equals