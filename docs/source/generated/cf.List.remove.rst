cf.List.remove
==============

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.List.remove