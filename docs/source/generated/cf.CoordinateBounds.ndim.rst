cf.CoordinateBounds.ndim
========================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.CoordinateBounds.ndim