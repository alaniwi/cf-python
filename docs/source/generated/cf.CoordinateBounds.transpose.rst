cf.CoordinateBounds.transpose
=============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateBounds.transpose