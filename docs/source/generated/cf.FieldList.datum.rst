cf.FieldList.datum
==================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.FieldList.datum