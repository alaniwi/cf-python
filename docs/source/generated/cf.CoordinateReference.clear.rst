cf.CoordinateReference.clear
============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateReference.clear