cf.CoordinateReference.update
=============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateReference.update