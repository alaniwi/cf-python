cf.Domain.axis_name
===================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Domain.axis_name