cf.AncillaryVariables.identity
==============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AncillaryVariables.identity