cf.AncillaryVariables.set_equals
================================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AncillaryVariables.set_equals