cf.FieldList.remove_item
========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.FieldList.remove_item