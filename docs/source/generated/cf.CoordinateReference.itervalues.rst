cf.CoordinateReference.itervalues
=================================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateReference.itervalues