cf.Domain.equivalent_refs
=========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Domain.equivalent_refs