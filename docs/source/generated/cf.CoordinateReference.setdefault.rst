cf.CoordinateReference.setdefault
=================================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateReference.setdefault