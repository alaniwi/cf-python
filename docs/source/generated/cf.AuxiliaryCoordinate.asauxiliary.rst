cf.AuxiliaryCoordinate.asauxiliary
==================================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AuxiliaryCoordinate.asauxiliary