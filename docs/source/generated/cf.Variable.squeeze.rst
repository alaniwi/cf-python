cf.Variable.squeeze
===================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Variable.squeeze