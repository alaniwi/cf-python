cf.CoordinateBounds.asreftime
=============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateBounds.asreftime