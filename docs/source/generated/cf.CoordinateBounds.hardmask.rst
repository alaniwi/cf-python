cf.CoordinateBounds.hardmask
============================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.CoordinateBounds.hardmask