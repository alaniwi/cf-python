cf.CellMethods.axes
===================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.CellMethods.axes