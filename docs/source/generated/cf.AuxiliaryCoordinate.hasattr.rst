cf.AuxiliaryCoordinate.hasattr
==============================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AuxiliaryCoordinate.hasattr