cf.CellMethods.comment
======================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.CellMethods.comment