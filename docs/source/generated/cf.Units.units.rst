cf.Units.units
==============

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Units.units