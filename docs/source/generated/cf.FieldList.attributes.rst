cf.FieldList.attributes
=======================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.FieldList.attributes