cf.Variable.isscalar
====================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Variable.isscalar