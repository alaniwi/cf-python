cf.Coordinate.missing_value
===========================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Coordinate.missing_value