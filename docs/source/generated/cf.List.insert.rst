cf.List.insert
==============

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.List.insert