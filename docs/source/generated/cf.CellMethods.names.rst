cf.CellMethods.names
====================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.CellMethods.names