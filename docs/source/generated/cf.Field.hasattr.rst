cf.Field.hasattr
================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Field.hasattr