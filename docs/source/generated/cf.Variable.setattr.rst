cf.Variable.setattr
===================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Variable.setattr