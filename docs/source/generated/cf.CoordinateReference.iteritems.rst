cf.CoordinateReference.iteritems
================================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateReference.iteritems