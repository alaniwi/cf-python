cf.Field.axes_sizes
===================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Field.axes_sizes