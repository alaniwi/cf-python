cf.Variable.hardmask
====================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Variable.hardmask