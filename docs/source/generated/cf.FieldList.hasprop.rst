cf.FieldList.hasprop
====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.FieldList.hasprop