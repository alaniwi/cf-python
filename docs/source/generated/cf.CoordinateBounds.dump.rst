cf.CoordinateBounds.dump
========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateBounds.dump