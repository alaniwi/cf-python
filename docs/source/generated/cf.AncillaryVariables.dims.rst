cf.AncillaryVariables.dims
==========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.AncillaryVariables.dims