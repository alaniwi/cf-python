cf.Variable.setdata
===================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Variable.setdata