cf.Variable.identity
====================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Variable.identity