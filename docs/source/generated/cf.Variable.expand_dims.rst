cf.Variable.expand_dims
=======================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Variable.expand_dims