cf.Field.getattr
================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Field.getattr