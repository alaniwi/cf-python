cf.Coordinate.upper_bounds
==========================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Coordinate.upper_bounds