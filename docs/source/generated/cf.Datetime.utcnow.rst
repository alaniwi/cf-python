cf.Datetime.utcnow
==================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.Datetime.utcnow