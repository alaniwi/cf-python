cf.CoordinateBounds.equals
==========================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CoordinateBounds.equals