cf.Variable.properties
======================

.. currentmodule:: cf
.. default-role:: obj

.. autoattribute:: cf.Variable.properties