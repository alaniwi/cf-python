cf.CellMethods.inspect
======================

.. currentmodule:: cf
.. default-role:: obj

.. automethod:: cf.CellMethods.inspect